/*
 * Copyright (c) 2016-2017  Moddable Tech, Inc.
 *
 *   This file is part of the Moddable SDK.
 *
 *   This work is licensed under the
 *       Creative Commons Attribution 4.0 International License.
 *   To view a copy of this license, visit
 *       <http://creativecommons.org/licenses/by/4.0>.
 *   or send a letter to Creative Commons, PO Box 1866,
 *   Mountain View, CA 94042, USA.
 *
 */

import Console from "console";
import CLI from "cli";
import Analog from "pins/analog";
import Digital from "pins/digital";
import PWM from "pins/pwm";
import Preference from "preference";

var ALLOWED_DIGITAL_IN  = 1;
var ALLOWED_DIGITAL_OUT = 2;
var ALLOWED_ANALOG_IN   = 4;
var ALLOWED_PWM         = 8;

var MODE = {
    NOT_SET : {
        label : function() {
            return '<null>';
        },
        mode : function() {
            return null;
        },
        modeAllowed : function( pin ) {
            return true;
        },
        factory : function( pin ) {
            return null;
        }
    },
    DIGITAL_IN : {
        label : function() {
            return "in";
        },
        mode : function() {
            return "in";
        },
        modeAllowed : function( pin ) {
            return pins[pin].allowed & ALLOWED_DIGITAL_IN;
        },
        factory : function( pin ) {
            return new Digital( { pin : pin, mode : Digital.Input } );
        },
        read : function( pin, that ) {
            if( that != null ) {
                that.line( `Reading DIGITAL_IN from pin ${pin}` );
            }
            var value = new Digital( { pin : pin, mode: Digital.Input } ).read();
            return value;
        }
    },
    DIGITAL_OUT : {
        label : function() {
            return "out";
        },
        mode : function() {
            return "out";
        },
        modeAllowed : function( pin ) {
            return pins[pin].allowed & ALLOWED_DIGITAL_OUT;
        },
        factory : function( pin ) {
            return new Digital( { pin : pin, mode : Digital.Output } );
        },
        isValidValue : function( value ) {
            value = parseInt( value, 10 );
            return value == 0 || value == 1;
        },
        write : function( pin, value, that ) {
            if( that != null ) {
                that.line( `Write DIGITAL_OUT ${value} to pin ${pin}` );
            }
            new Digital( { pin : pin, mode : Digital.Output } ).write( value );
        }
    },
    PWM : {
        label : function() {
            return "pwm";
        },
        mode : function() {
            return "pwm";
        },
        modeAllowed : function( pin ) {
            return pins[pin].allowed & ALLOWED_PWM;
        },
        factory : function( pin ) {
            return new PWM( { pin : pin } );
        },
        isValidValue : function( value ) {
            value = parseInt( value, 10 );
            return value >= 0 && value < 1024;
        },
        write : function( pin, value, that ) {
            if( that != null ) {
                that.line( `Write PWM ${value} to pin ${pin}` );
            }
            new PWM( { pin : pin } ).write( value );
        }
    },
    ANALOG_IN : {
        label : function() {
            return "analog";
        },
        mode : function() {
            return "analog";
        },
        modeAllowed : function( pin ) {
            return pins[pin].allowed & ALLOWED_ANALOG_IN;
        },
        factory : function( pin ) {
            var channel = pins[pin].adcchannel;
            return new Analog(  { pin : channel } );
        },
        read : function( pin, that ) {
            var channel = pins[pin].adcchannel;
            if( that != null ) {
                that.line( `Reading ANALOG_IN from pin ${pin} (ADC2 channel ${channel})` );
            }
            var value = Analog.read( channel );
            return value;
        }
    }
};

var preferenceName = 'pins';
const pins = new Array(40); // pins[i] = { mode : label, value : value }

function findMode( mode ) {
    for( var key in MODE ) {
        if( mode == MODE[key].mode() ) {
            return MODE[key];
        }
    }
    return null;
}

function isValidPin(that, index) {
    index = parseInt( index, 10 );
    return( index >= 0 && index < pins.length );
}

function cmdMode( that, params ) {
    if( params.length < 1 || params.length > 3 ) {
        that.line( `Missing parameter`);
        return false;
    }
    if( params.length == 1 ) {
        // print out all current modes
        for( var i=0 ; i<pins.length ; ++i ) {
            if( 'value' in pins[i] ) {
                that.line( `pin ${i}: ${pins[i].mode.label()}, value ${pins[i].value}` );
            } else {
                that.line( `pin ${i}: ${pins[i].mode.label()}` );
            }
        }

    } else {
        var pin = params[1];

        if( !isValidPin( that, pin )) {
            that.line( `Invalid pin number`);
            return false;
        }
        if( params.length == 2 ) {
            // print current mode
            that.line( `Mode: ${pins[pin].mode.label()}` );

        } else {
            // set mode
            var modeLabel = params[2];

            var found = 0;
            for( var mode in MODE ) {
                if( modeLabel == MODE[mode].label() ) {
                    if( MODE[mode].modeAllowed( pin )) {
                        that.line( `Setting pin ${pin} to ${MODE[mode].label()}`);
                        pins[pin].mode  = MODE[mode];
                        pins[pin].mode.factory( pin );
                        delete pins[pin].value;
                    } else {
                        that.line( `Mode not allowed on pin ${pin}`);
                    }
                    found = 1;
                    break;
                }
            }
            if( !found ) {
                that.line( `Invalid pin mode: ${modeLabel}`);
                return false;
            }
        }
    }

    return true;
}

function cmdWrite(that, params ) {
    if( params.length != 3 ) {
        that.line( `Missing parameter`);
        return false;
    }

    var pin   = params[1];
    var value = params[2];

    if( !isValidPin( that, pin )) {
        that.line( `Invalid pin number`);
        return false;
    }
    if( 'write' in pins[pin].mode ) {
        if( pins[pin].mode.isValidValue( value )) {
            pins[pin].mode.write( pin, value, that );
            pins[pin].value = value;

        } else {
            that.line( `Invalid value for this pin` );
            return false;
        }
    } else {
        that.line( `Wrong mode for pin ${pin}: ${pins[pin].mode.label()}` );
        return false;
    }

    return true;
}

function cmdRead( that, params ) {
    if( params.length != 2 ) {
        that.line( `Missing parameter`);
        return false;
    }

    var pin = params[1];

    if( !isValidPin( that, pin )) {
        that.line( `Invalid pin number`);
        return false;
    }

    if( 'read' in pins[pin].mode ) {
        var value = pins[pin].mode.read( pin, that );
        that.line( `Value: ${value}` );

    } else {
        that.line( `Wrong mode for pin ${pin}: ${pins[pin].mode.label()}` );
        return false;
    }
    return true;
}

function load( that, params ) {

    var pref = Preference.get( preferenceName, preferenceName );

    if( pref != null ) {
        if( that != null ) {
            that.line( `Preference exists, loading: ${pref}` );
        }
        var json = JSON.parse( pref );

        for( var i=0 ; i<pins.length ; ++i ) {
            if( i<json.state.length ) {
                var mode = findMode( json.state[i].mode );

                pins[i].mode = mode;
                if( 'value' in json.state[i] ) {
                    pins[i].mode.write( i, json.state[i]['value'], that );
                }
            }
        }
    } else {
        if( that != null ) {
            that.line( `Preference does not exist, not loading` );
        }
    }
    return true;
}

function cmdSave( that, params ) {
    var json = { state : [] };
    for( var i=0 ; i<pins.length ; ++i ) {
        var item = { mode : pins[i].mode.mode() };
        if( 'value' in pins[i] ) {
            item['value'] = pins[i]['value'];
        }
        json.state.push( item );
    }
    var content = JSON.stringify(json);

    Preference.set( preferenceName, preferenceName, content );

    if( that != null ) {
        that.line( `Wrote JSON:` );
        that.line( content );
    }
    return true;
}


CLI.install(function (command, params) {
    if( command != 'prompt' ) {
        var echo = "> " + command;
        if( params.length > 0 ) {
            echo += ` ` + params.join(` `);
        }
        this.line( echo );
    }

    switch (command) {

        case "gpio":
            var f = null;
            if( params.length > 0 ) {
                if( `mode` == params[0] ) {
                    f = cmdMode;

                } else if( `write` == params[0] ) {
                    f = cmdWrite;

                } else if( `read` == params[0] ) {
                    f = cmdRead;

                } else if( `load` == params[0] ) {
                    f = load; // this only exists for debugging

                } else if( `save` == params[0] ) {
                    f = cmdSave;
                }
            }
            if( f == null ) {
                this.line(`Invalid subcommand`);
            } else if( f( this, params )) {
                this.line(`OK`);
            } else {
                this.line(`Syntax error`);
            }
            break;

        case "help":
            this.line(`gpio mode`);
            this.line(`gpio mode <pin>`);
            this.line(`gpio mode <pin> [ in | out | pwm | analog ]`);
            this.line(`gpio write <pin> <value>`);
            this.line(`gpio read <pin>`);
            this.line(`gpio load (for debugging only)`);
            this.line(`gpio save`);
            break;

        default:
            return false;
    }
    return true;
});



for( var i=0 ; i<pins.length ; ++i ) {
    pins[i] = {
        mode    : MODE.NOT_SET,
        allowed : 0
    };
}

// set some metadata according to https://randomnerdtutorials.com/esp32-pinout-reference-gpios/
// err on the careful side

pins[2].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM;

pins[4].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM;

pins[5].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM;

for( var i=13 ; i<=31 ; ++i ) {
    pins[i].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM;
}

pins[32].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM | ALLOWED_ANALOG_IN;
pins[32].adcchannel = 4;

pins[33].allowed = ALLOWED_DIGITAL_IN | ALLOWED_DIGITAL_OUT | ALLOWED_PWM | ALLOWED_ANALOG_IN;
pins[33].adcchannel = 5;

pins[34].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[34].adcchannel = 6;

pins[35].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[35].adcchannel = 7;

pins[36].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[36].adcchannel = 0;

pins[37].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[37].adcchannel = 1;

pins[38].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[38].adcchannel = 2;

pins[39].allowed = ALLOWED_DIGITAL_IN | ALLOWED_ANALOG_IN;
pins[39].adcchannel = 3;

// load state from flash
load( null, null );

let console = new Console;

